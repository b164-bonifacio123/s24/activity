// ITEM 2
db.getCollection('users').find({ 
    $or:[
    
        {firstName:{$regex:'s', $options:'$i'}},
        {lastName:{$regex:'d', $options:'$i'}}
        
        ] 
    }, 
    {
        firstName:1,
        lastName:1,
        _id:0
    })




// ITEM 3
db.getCollection('users').find({
    $and:[
    {
        department:"HR"
    }, 
    {
        age:{$gte:70}
    }
    ]
})




// ITEM 4
db.getCollection('users').find({
    $and:[
    {
        firstName:{$regex: 'e', $options:'$i'}
    },
    {
        age:{$lte:30}
    }
    
    ]
})
